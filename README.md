Repository holding configurations for neovim

Before you use you must install neovim package manager, here is the command for that:
``````
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```````

