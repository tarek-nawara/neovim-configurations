call plug#begin()
Plug 'vim-airline/vim-airline'
Plug 'tpope/vim-fugitive'
Plug 'vim-airline/vim-airline-themes'
Plug 'icymind/NeoSolarized'
Plug 'Yggdroot/indentLine'
Plug 'morhetz/gruvbox'
Plug 'scrooloose/nerdtree'
Plug 'jiangmiao/auto-pairs'
Plug 'sheerun/vim-polyglot'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

" call PlugInstall to install new plugins
call plug#end()

" Basic
filetype plugin indent on
set tabstop=2
set shiftwidth=2
set expandtab
set number

set laststatus=0

vnoremap <leader>y "+y
nnoremap <leader>Y "+yg_
nnoremap <leader>p "+p


" set listchars=trail:.
" set list

" Theme
set background=dark
set termguicolors
colorscheme gruvbox

" FZF mappings
nnoremap <silent> <C-f> :Files<CR>
nnoremap <silent> <Leader>f :Rg<CR>
